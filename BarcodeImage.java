
public class BarcodeImage implements Cloneable{

	public static final int MAX_HEIGHT = 30;   
	public static final int MAX_WIDTH = 65;
	private boolean image_data[][];
	
	//Default constructor
	public BarcodeImage() {
		
		image_data = new boolean[MAX_HEIGHT][MAX_WIDTH];
		
		for(int row = 0; row < MAX_HEIGHT; row++)
			for(int col = 0; col < MAX_WIDTH; col++)
				image_data[row][col] = false;
		
	}
	
	//constructor with string array input
	public BarcodeImage(String[] str_data) {
		
		image_data = new boolean[MAX_HEIGHT][MAX_WIDTH];
		int strPos = str_data.length - 1;
		
		//Set all to false (blank)
		for(int row = 0; row < MAX_HEIGHT; row++)
			for(int col = 0; col < MAX_WIDTH; col++)
				image_data[row][col] = false;
		
		//Build image_data based on str_data. Will cut off anything bigger than the max limits)
		for(int row = MAX_HEIGHT-1; row > MAX_HEIGHT-str_data.length-1 && row >= 0; row--, strPos--) {
			
			for (int col = 0; col < str_data[strPos].length() && col < MAX_WIDTH; col++) {
				
				if(str_data[strPos].charAt(col) == DataMatrix.BLACK_CHAR)
					image_data[row][col] = true;
				else
					image_data[row][col] = false;
				
			}
			
			if(strPos < 0)
				row = -1;
			
		}
		
	}
	
	//Accessor-----------------------------
	boolean GetPixel(int row, int col) {
		
		if (row < 0 || row >= MAX_HEIGHT || col < 0 || col >= MAX_WIDTH) 
			return false; // use as an error 
		
		return this.image_data[row][col];
		
	}
	
	//Mutator------------------------------
	boolean SetPixel(int row, int col, boolean value) {
		
		if (row < 0 || row >= MAX_HEIGHT || col < 0 || col >= MAX_WIDTH) 
			return false;
		
		image_data[row][col] = value;
		return true;
		
	}
	
	//Implement clone()
	public Object clone() throws CloneNotSupportedException
   {

      BarcodeImage new_object = (BarcodeImage)super.clone();

      new_object.image_data = new boolean[MAX_HEIGHT][MAX_WIDTH];
      
      for(int row = 0; row < MAX_HEIGHT; row++)
			for(int col = 0; col < MAX_WIDTH; col++)
				new_object.image_data[row][col] = this.image_data[row][col];
      
      return new_object;
      
   }
	
	public void DisplayToConsole() {
		
		//Print top border
		for(int i = 0; i < FindActualWidth()+2; i++)
			System.out.print("-");
		System.out.println();
		
		//Print entire 2D image data array
		for(int row = MAX_HEIGHT - FindActualHeight(); row < MAX_HEIGHT; row++) {
			
			//Print left border
			System.out.print("|");
			
			for(int col = 0; col < FindActualWidth(); col++) {
				
				if(image_data[row][col])
					System.out.print(DataMatrix.BLACK_CHAR);
				else
					System.out.print(DataMatrix.WHITE_CHAR);
				
			}

			System.out.println("|");
			
		}
		
		//Print bottom border
		for(int i = 0; i < FindActualWidth()+2; i++)
			System.out.print("-");
		System.out.println();
		
	}
	
	//Find actual width of image
	public int FindActualWidth() {
		
		int count = 0;
		
		for(int i = 0; i < MAX_WIDTH; i++)
			if(image_data[MAX_HEIGHT-1][i])
				count++;
			
		return count;
		
	}
	
	//Find actual height of image
	public int FindActualHeight() {
		
		int count = 0;
		
		for(int i = MAX_HEIGHT - 1; i >= 0; i--)
			if(image_data[i][0])
				count++;
		
		return count;
		
	}
	
}
