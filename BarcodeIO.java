
public interface BarcodeIO {

	public boolean Scan(BarcodeImage bc);
	public boolean ReadText(String text);
	public boolean GenerateImageFromText();
	public boolean TranslateImageToText();
	public void DisplayTextToConsole();
	public void DisplayImageToConsole();
	
}
