
public class Foothill {

	public static void main(String[] args) {
		
		 String[] s_image_in = 
	      { 
	            "                                      ",
	            "                                      ",
	            "                                      ",
	            "* * * * * * * * * * * * * * * * *     ",
	            "*                                *    ",
	            "**** * ****** ** ****** *** ****      ",
	            "* ********************************    ",
	            "*    *   *  * *  *   *  *   *  *      ",
	            "* **    *      *   **    *       *    ",
	            "****** ** *** **  ***** * * *         ",
	            "* ***  ****    * *  **        ** *    ",
	            "* * *   * **   *  *** *   *  * **     ",
	            "**********************************    "
	      };
	      
	      String[] s_image_in_2 = 
	      { 
	            "                                          ",
	            "                                          ",
	            "* * * * * * * * * * * * * * * * * * *     ",
	            "*                                    *    ",
	            "**** *** **   ***** ****   *********      ",
	            "* ************ ************ **********    ",
	            "** *      *    *  * * *         * *       ",
	            "***   *  *           * **    *      **    ",
	            "* ** * *  *   * * * **  *   ***   ***     ",
	            "* *           **    *****  *   **   **    ",
	            "****  *  * *  * **  ** *   ** *  * *      ",
	            "**************************************    "
	      };
	      
	      BarcodeImage bc = new BarcodeImage(s_image_in);
	      DataMatrix dm = new DataMatrix(bc);
	     
	      // First secret message
	      dm.TranslateImageToText();
	      dm.DisplayTextToConsole();
	      dm.DisplayImageToConsole();	      
	      
	      // second secret message
	      bc = new BarcodeImage(s_image_in_2);
	      dm.Scan(bc);
	      dm.TranslateImageToText();
	      dm.DisplayTextToConsole();
	      dm.DisplayImageToConsole();
	      
	      // create your own message
	      dm.ReadText("CIS 27B rocks more than Zeppelin");
	      dm.GenerateImageFromText();
	      dm.DisplayTextToConsole();
	      dm.DisplayImageToConsole();
	      	      
	}

}
