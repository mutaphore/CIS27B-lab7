
public class DataMatrix implements BarcodeIO{
	
	public static final char BLACK_CHAR = '*';
	public static final char WHITE_CHAR = ' ';
	public static final String DEFAULT_TEXT = "undefined text";
	private BarcodeImage image;
	private String text;
	private int actual_width;
	private int actual_height;
	
	public DataMatrix() {
		
		actual_width = 0;
		actual_height = 0;
		text = DEFAULT_TEXT;
		image = new BarcodeImage();
		
	}
	
	public DataMatrix(BarcodeImage image) {
		
		if(!Scan(image)) 
			this.image = new BarcodeImage();
		
		text = DEFAULT_TEXT;
		
	}
	
	public DataMatrix(String text) {
		
		if(!ReadText(text))
			this.text = DEFAULT_TEXT;
		
		image = new BarcodeImage();
		
	}
	
	//Mutators-------------------------------
	public boolean ReadText(String text) {
		
		if(text.length() > BarcodeImage.MAX_WIDTH)
			return false;
		else {
			this.text = text;
			return true;
		}
		
	}
	
	//Call clone() to create copy of BarcodeImage
	public boolean Scan(BarcodeImage image) {
		
		try{	
			this.image = (BarcodeImage)image.clone();
		}
		catch(CloneNotSupportedException e) {
			System.out.print(e.getMessage());
			return false;
		}
		
		actual_width = image.FindActualWidth();
		actual_height = image.FindActualHeight();
		return true;
		
	}
	
	//Accessors--------------------------------
	public int GetActualWidth() {
		
		return actual_width;
		
	}
	
	public int GetActualHeight() {
	
		return actual_height;
	
	}
	
	
	//Find signal width based on text length
	private int ComputeSignalWidth() {
		
		return text.length()+2;
		
	}
	
	//Find what's the maximum number of bits required to represent text
	private int ComputeSignalHeight() {
		
		int max = 0;
		int bits = 0;
		
		for(int i = 0; i < text.length(); i++)
			if(max < (int)text.charAt(i))
				max = (int)text.charAt(i);
		
		while(max - (1 << bits) > 0)
			bits++;
		
		return bits+3;
		
	}
	
	//Convert text to BarcodeImage
	public boolean GenerateImageFromText() {
			
		if(ComputeSignalWidth() > BarcodeImage.MAX_WIDTH || ComputeSignalHeight() > BarcodeImage.MAX_HEIGHT)
			return false;
		else {
			image = new BarcodeImage();
			GenerateBorders();
	
			for(int i = 1; i < ComputeSignalWidth()-1; i++)
				WriteCharToCol(i,(int)text.charAt(i-1));
			
			actual_height = ComputeSignalHeight();
			actual_width = ComputeSignalWidth();
			
			return true;
		}
			
	}
	
	//Convert BarcodeImage to text
	public boolean TranslateImageToText() {
		
		StringBuffer temp = new StringBuffer();
		
		if(image.FindActualHeight() == 0 || image.FindActualWidth() == 0)
			return false;
		else {
			for(int i = 1; i < image.FindActualWidth() - 1; i++)
				temp.append(ReadCharFromCol(i));
			text = temp.toString();
			return true;	
		}
		
	}
	
	//Generates a column in Barcode image based on the int value of 
	//character in text
	private boolean WriteCharToCol(int col, int code) {
		
		int bit_to_examine, bit_result;
		
		if(col < 0 || col > BarcodeImage.MAX_WIDTH - 1)
			return false;
				
		for (int k = 0; k < ComputeSignalHeight() - 2; k++) {
			
         bit_to_examine = 1 << k;
         bit_result = bit_to_examine & code;
         
         if ( bit_result != 0 )
            image.SetPixel(BarcodeImage.MAX_HEIGHT - k - 2, col, true);
         else
            image.SetPixel(BarcodeImage.MAX_HEIGHT - k - 2, col, false);
         
      }
		
		return true;
		
	}
	
	//Creates the 4 borders in a signal, the closed limitation lines 
	//and open border lines
	private void GenerateBorders(){
		
		//Generate bottom closed limitation line
		for(int j = 0; j < text.length() + 2; j++)
			image.SetPixel(BarcodeImage.MAX_HEIGHT - 1, j, true);
		
		//Generate left closed limitation line
		for(int i = BarcodeImage.MAX_HEIGHT - 1; i >= BarcodeImage.MAX_HEIGHT - ComputeSignalHeight(); i--)
			image.SetPixel(i, 0, true);
		
		//Generate right open border line
		for(int i = 0; i <  ComputeSignalHeight(); i++) {
			
			if(i%2 == 0)
				image.SetPixel(BarcodeImage.MAX_HEIGHT - i - 1, ComputeSignalWidth() - 1, true);
			else
				image.SetPixel(BarcodeImage.MAX_HEIGHT - i - 1, ComputeSignalWidth() - 1, false);
		
		}
		
		//Generate top open border line
		for(int j = 0; j < text.length() + 2; j++) {
		
			if(j%2 == 0)
				image.SetPixel(BarcodeImage.MAX_HEIGHT - ComputeSignalHeight(), j, true);
			else
				image.SetPixel(BarcodeImage.MAX_HEIGHT - ComputeSignalHeight(), j, false);
		
		}
		
	}
	
	//Converts a specified column in BarcodeImage and
	//returns the char value
	private char ReadCharFromCol(int col) {
		
		int sum = 0;
	 		
		for (int k = BarcodeImage.MAX_HEIGHT - 2; k > BarcodeImage.MAX_HEIGHT - image.FindActualHeight() ; k--) {
			
			if(image.GetPixel(k, col)) {
				sum += (1 << (BarcodeImage.MAX_HEIGHT - k - 2));
			}
		}
				
		return (char)sum;
		
	}
	
	//Print DM text
	public void DisplayTextToConsole() {
		
		System.out.println(text);
		
	}
	
	//Print DM BarcodeImage
	public void DisplayImageToConsole() {
		
		image.DisplayToConsole();
		
	}

}
